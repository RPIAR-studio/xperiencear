﻿using UnityEngine;
using System.Collections;
using Vuforia;
// using TMPro;
using UnityEngine.Events;

public class CustomTrackableEventHandler : MonoBehaviour
{
    [SerializeField] bool _isSwitchableMat;
    [SerializeField] GameObject _goPlaneIndicator, _goMainObject, _goRenderCamera, _goParticles, _goRC;

    bool _isOnTracking, _isMarkerFound, _isColoringMode, _isCaptured;
    float _countdownCapture = 3;

    public enum TrackingStatusFilter
    {
        Tracked,
        Tracked_ExtendedTracked,
        Tracked_ExtendedTracked_Limited
    }

    public TrackingStatusFilter StatusFilter = TrackingStatusFilter.Tracked_ExtendedTracked_Limited;

    protected TrackableBehaviour mTrackableBehaviour;
    protected TrackableBehaviour.Status m_PreviousStatus;
    protected TrackableBehaviour.Status m_NewStatus;
    protected TrackableBehaviour.StatusInfo m_PreviousStatusInfo;
    protected TrackableBehaviour.StatusInfo m_NewStatusInfo;
    protected bool m_CallbackReceivedOnce = false;


    [Space()]
    public string bookID;
    public UnityEvent _customEventOnFound;
    public UnityEvent _customEventOnLost;

    protected virtual void Start()
    {
        _isColoringMode = (_goRenderCamera != null || _goPlaneIndicator != null);
        
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();

        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterOnTrackableStatusChanged(OnTrackableStatusChanged);
            mTrackableBehaviour.RegisterOnTrackableStatusInfoChanged(OnTrackableStatusInfoChanged);
        }
    }

    protected virtual void OnDestroy()
    {
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.UnregisterOnTrackableStatusInfoChanged(OnTrackableStatusInfoChanged);
            mTrackableBehaviour.UnregisterOnTrackableStatusChanged(OnTrackableStatusChanged);
        }
    }

    void OnTrackableStatusChanged(TrackableBehaviour.StatusChangeResult statusChangeResult)
    {
        m_PreviousStatus = statusChangeResult.PreviousStatus;
        m_NewStatus = statusChangeResult.NewStatus;

        Debug.LogFormat("Trackable {0} {1} -- {2}",
            mTrackableBehaviour.TrackableName,
            mTrackableBehaviour.CurrentStatus,
            mTrackableBehaviour.CurrentStatusInfo);

        HandleTrackableStatusChanged();
    }

    void OnTrackableStatusInfoChanged(TrackableBehaviour.StatusInfoChangeResult statusInfoChangeResult)
    {
        m_PreviousStatusInfo = statusInfoChangeResult.PreviousStatusInfo;
        m_NewStatusInfo = statusInfoChangeResult.NewStatusInfo;
        
        HandleTrackableStatusInfoChanged();
    }

    protected virtual void HandleTrackableStatusChanged()
    {
        if (!ShouldBeRendered(m_PreviousStatus) &&
            ShouldBeRendered(m_NewStatus))
        {
            OnTrackingFound();
        }
        else if (ShouldBeRendered(m_PreviousStatus) &&
                 !ShouldBeRendered(m_NewStatus))
        {
            OnTrackingLost();
        }
        else
        {
            if (!m_CallbackReceivedOnce && !ShouldBeRendered(m_NewStatus))
            {
                // This is the first time we are receiving this callback, and the target is not visible yet.
                // --> Hide the augmentation.
                OnTrackingLost();
            }
        }

        m_CallbackReceivedOnce = true;
    }

    protected virtual void HandleTrackableStatusInfoChanged()
    {
        if (m_NewStatusInfo == TrackableBehaviour.StatusInfo.WRONG_SCALE)
        {
            Debug.LogErrorFormat("The target {0} appears to be scaled incorrectly. " + 
                                 "This might result in tracking issues. " + 
                                 "Please make sure that the target size corresponds to the size of the " + 
                                 "physical object in meters and regenerate the target or set the correct " +
                                 "size in the target's inspector.", mTrackableBehaviour.TrackableName);
        }
    }

    protected bool ShouldBeRendered(TrackableBehaviour.Status status)
    {
        if (status == TrackableBehaviour.Status.DETECTED ||
            status == TrackableBehaviour.Status.TRACKED)
        {
            // always render the augmentation when status is DETECTED or TRACKED, regardless of filter
            return true;
        }

        if (StatusFilter == TrackingStatusFilter.Tracked_ExtendedTracked)
        {
            if (status == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                // also return true if the target is extended tracked
                return true;
            }
        }

        if (StatusFilter == TrackingStatusFilter.Tracked_ExtendedTracked_Limited)
        {
            if (status == TrackableBehaviour.Status.EXTENDED_TRACKED ||
                status == TrackableBehaviour.Status.LIMITED)
            {
                // in this mode, render the augmentation even if the target's tracking status is LIMITED.
                // this is mainly recommended for Anchors.
                return true;
            }
        }

        return false;
    }

    // protected virtual void Start()
    // {
    //     _isColoringMode = (_goRenderCamera != null || _goPlaneIndicator != null);

    //     mTrackableBehaviour = GetComponent<TrackableBehaviour>();
    //     if (mTrackableBehaviour)
    //         mTrackableBehaviour.RegisterTrackableEventHandler(this);
    // }

    // protected virtual void OnDestroy()
    // {
    //     if (mTrackableBehaviour)
    //         mTrackableBehaviour.UnregisterTrackableEventHandler(this);
    // }

    // public void OnTrackableStateChanged(
    //     TrackableBehaviour.Status previousStatus,
    //     TrackableBehaviour.Status newStatus)
    // {
    //     m_PreviousStatus = previousStatus;
    //     m_NewStatus = newStatus;

    //     if (newStatus == TrackableBehaviour.Status.DETECTED ||
    //         newStatus == TrackableBehaviour.Status.TRACKED ||
    //         newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
    //     {
    //         OnTrackingFound();
    //     }
    //     else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
    //              newStatus == TrackableBehaviour.Status.NO_POSE)
    //     {
    //         OnTrackingLost();
    //     }
    //     else
    //     {
    //         OnTrackingLost();
    //     }
    // }

    protected virtual void OnTrackingFound()
    {
        if (_customEventOnFound != null)
            _customEventOnFound.Invoke();

        _isOnTracking = true;

        if (_goRC != null)
            _goRC.SetActive(true);

        if (mTrackableBehaviour && ARModeManager.isInstructionDone)
        {
            _isMarkerFound = true;

            if (_isColoringMode)
            {
                _goPlaneIndicator.SetActive(_goRenderCamera.activeSelf);
                _goMainObject.SetActive(!_goRenderCamera.activeSelf);

                ARModeManager.init.SetActiveRecaptureBtn(!_goRenderCamera.activeSelf);
                ARModeManager.init.SetCurrentActiveRenderCamera((_goRenderCamera == null) ? null : _goRenderCamera);
            }
            else
            {
                _goMainObject.SetActive(true);

                ARModeManager.init.SetActiveSwitchTextureBtn(_isSwitchableMat);
                ARModeManager.init.SetCurrentActiveRenderCamera(null);
            }
        }
    }

    protected virtual void OnTrackingLost()
    {
        if (_customEventOnLost != null)
            _customEventOnLost.Invoke();

        _isOnTracking = false;

        if (mTrackableBehaviour && ARModeManager.isInstructionDone)
        {
            _isMarkerFound = false;

            foreach (Transform item in transform)
            {
                item.gameObject.SetActive(false);
            }
        }

        ARModeManager.init.SetTextCountdown("");

        _countdownCapture = 3;

        if (_goRC != null)
            _goRC.SetActive(false);

        ARModeManager.isMarkerOutOfBounds = false;

        ARModeManager.init.SetActiveSwitchTextureBtn(false);
        ARModeManager.init.SetActiveRecaptureBtn(false);
    }

    void Update()
    {
        // condition kalo stuck
        if (_isOnTracking && mTrackableBehaviour && ARModeManager.isInstructionDone && !_isMarkerFound)
        {
            OnTrackingFound();
        }

        if (!_isColoringMode)
                return;

        if (_isMarkerFound && ARModeManager.isInstructionDone && _goRenderCamera.activeSelf)
        {
            if (ARModeManager.isMarkerOutOfBounds)
            {
                _countdownCapture = 3;
                ARModeManager.init.SetTextCountdown("");

                return;
            }
                            

            if (_countdownCapture > 0)
            {
                _countdownCapture -= Time.deltaTime;

                // make sure clear
                _goMainObject.SetActive(false);

                // matiin dulu particle
                // if (_countdownCapture < 1.5f)
                // {
                //     GameObject temp = Instantiate(_goParticles, _goParticles.transform.position, _goParticles.transform.rotation, _goParticles.transform.parent);

                //     temp.SetActive(true);
                // }

                _goPlaneIndicator.SetActive(_countdownCapture >= 1f);

                ARModeManager.init.SetTextCountdown(_countdownCapture.ToString("f0"));
            }
            else
            {
                StartCoroutine(ShowMainObject());
            }
        }
        // else
        // {
        //     _textCountdown.text = "";
        //     _countdownCapture = 3;
        // }
    }

    IEnumerator ShowMainObject()
    {
        // save texture first
        _goRenderCamera.GetComponent<RenderTextureCamera>().MakeScreen(mTrackableBehaviour.TrackableName);

        yield return new WaitForSeconds(.5f);

        // reset buat next recapture
        ARModeManager.init.SetTextCountdown("");
        _countdownCapture = 3;

        _goMainObject.SetActive(true);
        _goRenderCamera.SetActive(false);

        ARModeManager.init.SetActiveRecaptureBtn((_isColoringMode) ? true : false);
    }

    
}
