﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ARModeManager : MonoBehaviour
{
    [System.Serializable]
    public struct BGM_Sources
    {
        public string bookID;
        public AudioClip clip;
    }

    public static ARModeManager init;
    public static bool isMarkerOutOfBounds, isInstructionDone;
    public static Transform currentTargetObject;

    [SerializeField] GameObject _gOBaseAllMarkers, _gOInstruction, _gOScreenshotBtn, _goSwitchTextureBtn, _gORecaptureBtn, _gOBackBtn, _gOSplashEfx;
    [SerializeField] TMP_Text _textCountdown;
    [SerializeField] AudioSource _BGM;

    [Header("Settings")]
    [SerializeField] UnityEngine.UI.Image _imgMute;
    [SerializeField] Sprite[] _spritesMute;
    [SerializeField] GameObject _gOGroupSetting;
    [SerializeField] BGM_Sources[] _bgmSources;

    Texture2D _tempCapturedTexture;
    GameObject _gOCurRenderCamera;

    Coroutine _setBGMVolumeRoutine;

    private void Awake() 
    {
        init = this;
    }

    IEnumerator Start()
    {
        isInstructionDone = false;

        // check asset, downnload cache biasa dari bookID

        yield return null;

        // aktifin marker sesuai bookID
        yield return SetCurrentMarkerOnly();

        InitAudioBGM();
    }

    IEnumerator SetCurrentMarkerOnly()
    {
        // harus biarin aktif di awal biar ga auto assign marker
        yield return null;

        foreach (Transform item in _gOBaseAllMarkers.transform)
        {
            item.gameObject.SetActive(item.GetComponent<CustomTrackableEventHandler>().bookID == PlayerPrefs.GetString("bookID"));
            yield return null;
        }
    }

    public void GoToMainMenu()
    {
        LoaderSceneHandler.LoadScene("SC_MainMenu");
    }

    public void InstructionDone()
    {
        isInstructionDone = true;
        _gOInstruction.SetActive(false);
    }

    public void SetMarkerIsOutOfBounds(bool outOfBounds)
    {
        isMarkerOutOfBounds = outOfBounds;
    }

    public void SaveImageToGallery()
    {
        StartCoroutine(TakeScreenshot());
    }

    public void SetTargetObject(Transform trgtObj)
    {
        currentTargetObject = trgtObj;
    }

    public void SwitchCurrentTargetObjectMat()
    {
        ObjectMaterialSwitcher[] tempMatsSwitcher = currentTargetObject.GetComponents<ObjectMaterialSwitcher>();

        foreach (ObjectMaterialSwitcher tempMatSwitcher in tempMatsSwitcher)
        {   
            tempMatSwitcher.SwitchMats();
        }
    }

    #region SETTINGS
    void InitAudioBGM()
    {
        foreach (BGM_Sources item in _bgmSources)
        {
            if (item.bookID == PlayerPrefs.GetString("bookID") && item.clip != null)
            {
                _BGM.clip = item.clip;
            
                // check is it muted
                MuteBGM(PlayerPrefs.HasKey("IsMuted"));
                
                return;
            }
        }

        // if no clip available
        _imgMute.gameObject.SetActive(false);
    }

    public void ShowSetting()
    {
        _gOGroupSetting.SetActive(!_gOGroupSetting.activeSelf);
    }

    public void ShowInfo()
    {
        _gOInstruction.SetActive(!_gOInstruction.activeSelf);
    }

    public void MuteBGM()
    {
        MuteBGM(!PlayerPrefs.HasKey("IsMuted"));
    }

    void MuteBGM(bool isMuted)
    {
        _imgMute.sprite = _spritesMute[(isMuted) ? 0 : 1];

        if (isMuted) 
        {
            PlayerPrefs.SetInt("IsMuted", 1);
            
            _BGM.Stop();
        }
        else 
        {
            PlayerPrefs.DeleteKey("IsMuted");
            _BGM.Play();
        }
    }
    #endregion

    IEnumerator TakeScreenshot()
    {
        _gOScreenshotBtn.SetActive(false);
        _gOBackBtn.SetActive(false);

        PlayerPrefs.SetInt("CapturedIMG", PlayerPrefs.GetInt("CapturedIMG") + 1);

        yield return new WaitForEndOfFrame();

        _tempCapturedTexture = ScreenCapture.CaptureScreenshotAsTexture();

        NativeGallery.SaveImageToGallery(_tempCapturedTexture, "ExperienceAR", "SC_ExperienceAR_" + PlayerPrefs.GetInt("CapturedIMG") + ".jpg", null);

        StartCoroutine(TextureCapturedRefresh());
    }

    IEnumerator TextureCapturedRefresh()
    {
        _gOBackBtn.SetActive(true);
        _gOSplashEfx.SetActive(true);

        yield return new WaitForSeconds(2);

        Object.Destroy(_tempCapturedTexture);

        _gOScreenshotBtn.SetActive(true);
        _gOSplashEfx.SetActive(false);
    }

    public void SetCurrentActiveRenderCamera(GameObject currentRendCam)
    {
        _gOCurRenderCamera = currentRendCam;
    }

    public void SetActiveSwitchTextureBtn(bool isTrue)
    {
        _goSwitchTextureBtn.SetActive(isTrue);
    }

    public void SetActiveRecaptureBtn(bool isTrue)
    {
        _gORecaptureBtn.SetActive(isTrue);
    }

    public void RecaptureCurrentBook()
    {
        if (_gOCurRenderCamera != null)
            _gOCurRenderCamera.SetActive(true);
    }

    public void SetTextCountdown(string tCD)
    {
        _textCountdown.text = tCD;
    }

    public void SetBGMVolumeDown(float timer)
    {
        if (_setBGMVolumeRoutine != null)
            StopCoroutine(_setBGMVolumeRoutine);

        _setBGMVolumeRoutine = StartCoroutine(SetBGMVolumeDownRoutine(timer));
    }

    IEnumerator SetBGMVolumeDownRoutine(float timer)
    {
        _BGM.volume = .35f;

        yield return new WaitForSeconds(timer);

        _BGM.volume = 1f;

        // clear routine
        _setBGMVolumeRoutine = null;
    }
}
