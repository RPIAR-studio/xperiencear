﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ObjectMaterialSwitcher : MonoBehaviour
{
    [SerializeField] string _textureID;
    List<Renderer> _targetRenderer;
    // [SerializeField] List<Material> _coloringMats, _renderedMats;
    [SerializeField] Material _coloringMat, _renderedMat;
    
    [Header("Multiple mats setting")]
    [SerializeField] int _matIndex;
    [SerializeField] bool _allMats;
    [SerializeField] string _customTextureTag;

    bool _isColoring;

    // temp dev
    // [Space(20)]
    // [SerializeField] bool _switch;

    // void Update() 
    // {
    //     if (_switch)
    //     {
    //         SwitchMats();
    //         _switch = false;
    //     }
    // }
    // temp dev


    void OnEnable()
    {
        GetColoredTexture();
    }

    void GetColoredTexture()
    {
        string filePath = Application.persistentDataPath + "/" + _textureID + ".png";

        if (System.IO.File.Exists(filePath))
        {
            Texture2D texture = null;
            byte[] fileData;

            fileData = System.IO.File.ReadAllBytes(filePath);
            texture = new Texture2D(1024, 1024);

            texture.LoadImage(fileData);

            _coloringMat.mainTexture = texture;
        }

        StartCoroutine(GetAllTagSwitchable());
    }


    IEnumerator GetAllTagSwitchable()
    {
        _targetRenderer = new List<Renderer>();

        // liat dari yg ada RC_GetTexture
        foreach (SwitchableObjMatTag item in GetComponentsInChildren<SwitchableObjMatTag>(true))
        {
            if (item.textureID == _textureID)
            {
                if (!string.IsNullOrEmpty(_customTextureTag) && !string.IsNullOrEmpty(item.customTextureTag))
                {
                    if (item.customTextureTag == _customTextureTag)
                        _targetRenderer.Add(item.GetComponent<Renderer>());
                }
                else
                {
                    _targetRenderer.Add(item.GetComponent<Renderer>());
                }
            }

            yield return null;
        }
    }

    public void SwitchMats()
    {
        _isColoring = !_isColoring;

        foreach (Renderer item in _targetRenderer)
        {
            Material[] mats = item.materials;

            if (_allMats)
                for (int i = 0; i < mats.Length; i++)
                    mats[i] = (_isColoring) ? _coloringMat : _renderedMat;
            else
                mats[_matIndex] = (_isColoring) ? _coloringMat : _renderedMat;
            
            item.materials = mats;
        }
    }
}
