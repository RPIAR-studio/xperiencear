﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomRotate : MonoBehaviour
{
    bool _isRotating;
    
    Vector3 _mouseReference;
    float _rotVelocity;

    void RotateObject(Vector2 pos)
    {
        float _sensitivity = .25f;

        // if (Vector2.Distance(pos, new Vector2(Screen.width / 2, Screen.height / 2)) < Screen.height / 3)
        // {
            if (!_isRotating)
            {
                _isRotating = true;
                _mouseReference = Input.mousePosition;
            }
            else
            {
                Vector3 _mouseOffset = (Input.mousePosition - _mouseReference);
                _rotVelocity = -(_mouseOffset.x) * _sensitivity;
                ARModeManager.currentTargetObject.Rotate(0, _rotVelocity, 0);

                _mouseReference = Input.mousePosition;
            }
        // }
    }

    void CheckLastVelocity()
    {
        if (!Mathf.Approximately(_rotVelocity, 0))
        {
            float deltaVelocity = Mathf.Min(
                Mathf.Sign(_rotVelocity) * Time.deltaTime * ((_rotVelocity > 8) ? 250 : 100),
                Mathf.Sign(_rotVelocity) * _rotVelocity
            );

            _rotVelocity -= deltaVelocity;
            ARModeManager.currentTargetObject.Rotate(0, _rotVelocity, 0);
        }
    }
    
    void ZoomObject(Touch t1, Touch t2)
    {
        Vector2 touchZeroPrevPos = t1.position - t1.deltaPosition;
        Vector2 touchOnePrevPos = t2.position - t2.deltaPosition;

        float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
        float touchDeltaMag = (t1.position - t2.position).magnitude;

        // float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
        float deltaMagnitudeDiff = touchDeltaMag - prevTouchDeltaMag;


        float val = Mathf.Clamp((deltaMagnitudeDiff / (Screen.width / 4)), -1f, 1f);
        float speed = (((t1.deltaPosition.magnitude / t1.deltaTime) + (t2.deltaPosition.magnitude / t2.deltaTime)) / 2) / (Screen.width / 4);

        ZoomARSessionOrigin(new Vector3(val * speed, val * speed, val * speed));

        // reset rotate vel
        _rotVelocity = 0;
    }

    void ZoomARSessionOrigin(Vector3 scale)
    {
        ARModeManager.currentTargetObject.localScale += scale;

        //clamp scale
        ARModeManager.currentTargetObject.localScale = new Vector3(
            Mathf.Clamp(ARModeManager.currentTargetObject.localScale.x, .25f, 15f),
            Mathf.Clamp(ARModeManager.currentTargetObject.localScale.y, .25f, 15f),
            Mathf.Clamp(ARModeManager.currentTargetObject.localScale.z, .25f, 15f)
        );
    }

    void Update()
    {
        if (Input.touchCount == 1)
            RotateObject(Input.GetTouch(0).position);
        else if (Input.touchCount == 2 && Input.GetTouch(0).phase == TouchPhase.Moved && Input.GetTouch(1).phase == TouchPhase.Moved)
            ZoomObject(Input.GetTouch(0), Input.GetTouch(1));
        else
        {
            _isRotating = false;
            CheckLastVelocity();
        }
    }
}
