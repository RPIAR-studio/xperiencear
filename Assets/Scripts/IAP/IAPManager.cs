﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Networking;
using TMPro;

public class IAPManager : MonoBehaviour
{
    public static IAPManager init;

    [SerializeField] GameObject _IAPLoader;
    string _transactionID;

    void Awake()
    {
        init = this;
    }

    public void PurchaseSucceed(Product product)
    {
        // Debug.LogWarning("PURCHASED = " + PlayerPrefs.GetString("selectedProductID") + " ;;; " + product.transactionID);

        _transactionID = product.transactionID;

        // product.metadata.localizedPrice
        // product.receipt

        // send to our API
        SendPurchasedStatusAPI();
    }

    public void PurchaseFailed(Product product, PurchaseFailureReason reason)
    {
        Debug.LogWarning("FAILED = " + product.transactionID + "; REASON = " + reason);

        // check reason kalo duplicate order nanti save local buat tetep unlocked
        switch (reason.ToString())
        {
            case "DuplicateTransaction":
                // save local payment
                PlayerPrefs.SetInt(PlayerPrefs.GetString("bookID") + ConfigGeneral.IS_PURCHASED, 1);

                // refresh book
                LoaderSceneHandler.LoadScene("SC_MainMenu");
            break;
            default:
                // kalo gagal atau cancel make sure ke tutup aja
                _IAPLoader.SetActive(false);
            break;
        }
    }

    IEnumerator SendAPI(string url, string json, System.Action<UnityWebRequest> onSucceed)
    {
        using (UnityWebRequest uwr = new UnityWebRequest(url, "POST"))
        {
            byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);
            uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
            uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
            uwr.SetRequestHeader("Content-Type", "application/json");

            // Debug.LogError("SENDING ;;; \nURL = " + url + " ;;; \n json = " + json);

            yield return uwr.SendWebRequest();

            // Debug.LogError("SENT");

            if (uwr.isNetworkError)
            {
                Debug.LogError("Error While Sending: " + uwr.error);
                APIHandler.ConnectionErrorHandler();
            }
            else
            {
                ResponseGeneral responseIAP = ResponseGeneral.CreateFromJSON(uwr.downloadHandler.text);

                if (responseIAP.code == 200)
                {
                    onSucceed(uwr);
                }
                else
                {
                    Debug.LogError("error; " + responseIAP.code + "; " + responseIAP.messages);
                }
            }
        }
    }

    void SendPurchasedStatusAPI()
    {
        // show loading process
        _IAPLoader.SetActive(true);

        // send API
        StartCoroutine(
            SendAPI(
                ConfigGeneral.PREFIX_API_URL + "v1/book/buy", "{ \"id_token\":\"" + PlayerPrefs.GetString("id_token") + 
                    "\", \"company_id\":\"" + PlayerPrefs.GetString("companyID") + 
                    "\", \"book_id\":\"" + PlayerPrefs.GetString("bookID") + 
                    "\", \"payment_from\": \"" + ConfigGeneral.Platform() + 
                    "\", \"transaction_id\": \"" + _transactionID + "\" }", 
                    IAPBought
            )
        );
    }

    void IAPBought(UnityWebRequest uwr)
    {
        Debug.Log("Received = " + uwr.downloadHandler.text);

        // refresh book
        LoaderSceneHandler.LoadScene("SC_MainMenu");
    }
}
