#if UNITY_PURCHASING || UNITY_UNIFIED_IAP
using UnityEngine.Events;
using UnityEngine.UI;
using System.IO;
using System.Collections.Generic;

namespace UnityEngine.Purchasing
{
    // [RequireComponent(typeof(Button))]
    [AddComponentMenu("Unity IAP/IAP Button")]
    [HelpURL("https://docs.unity3d.com/Manual/UnityIAP.html")]
    public class IAPButton : MonoBehaviour
    {

        [System.Serializable]
        public class OnPurchaseCompletedEvent : UnityEvent<Product>
        {
        };

        [System.Serializable]
        public class OnPurchaseFailedEvent : UnityEvent<Product, PurchaseFailureReason>
        {
        };

        public string productId;
        public bool consumePurchase = true;
        public OnPurchaseCompletedEvent onPurchaseComplete;
        public OnPurchaseFailedEvent onPurchaseFailed;
        // public Text titleText;
        // public Text descriptionText;
        // public Text priceText;

        void Start()
        {
            // Button button = GetComponent<Button>();

            // if (buttonType == ButtonType.Purchase)
            // {
                // if (button)
                // {
                //     button.onClick.AddListener(PurchaseProduct);
                // }

                if (string.IsNullOrEmpty(productId))
                {
                    Debug.LogError("IAPButton productId is empty");
                }

                if (!CodelessIAPStoreListener.Instance.HasProductInCatalog(productId))
                {
                    Debug.LogWarning("The product catalog has no product with the ID \"" + productId + "\"");
                }
            // }
            // else if (buttonType == ButtonType.Restore)
            // {
                // if (button)
                // {
                //     button.onClick.AddListener(Restore);
                // }
            // }
        }

        void OnEnable()
        {
            // if (buttonType == ButtonType.Purchase)
            // {
                CodelessIAPStoreListener.Instance.AddButton(this);
                if (CodelessIAPStoreListener.initializationComplete) {
                    UpdateText();
                }
            // }
        }

        void OnDisable()
        {
            // if (buttonType == ButtonType.Purchase)
            // {
                CodelessIAPStoreListener.Instance.RemoveButton(this);
            // }
        }

        public void PurchaseProduct()
        {
            // if (buttonType == ButtonType.Purchase)
            // {
                Debug.Log("IAPButton.PurchaseProduct() with product ID: " + productId);

                CodelessIAPStoreListener.Instance.InitiatePurchase(productId);
            // }
        }

        // void Restore()
        // {
        //     if (buttonType == ButtonType.Restore)
        //     {
        //         if (Application.platform == RuntimePlatform.WSAPlayerX86 ||
        //             Application.platform == RuntimePlatform.WSAPlayerX64 ||
        //             Application.platform == RuntimePlatform.WSAPlayerARM)
        //         {
        //             CodelessIAPStoreListener.Instance.ExtensionProvider.GetExtension<IMicrosoftExtensions>()
        //                 .RestoreTransactions();
        //         }
        //         else if (Application.platform == RuntimePlatform.IPhonePlayer ||
        //                  Application.platform == RuntimePlatform.OSXPlayer ||
        //                  Application.platform == RuntimePlatform.tvOS)
        //         {
        //             CodelessIAPStoreListener.Instance.ExtensionProvider.GetExtension<IAppleExtensions>()
        //                 .RestoreTransactions(OnTransactionsRestored);
        //         }
        //         else if (Application.platform == RuntimePlatform.Android &&
        //                  StandardPurchasingModule.Instance().appStore == AppStore.SamsungApps)
        //         {
        //             CodelessIAPStoreListener.Instance.ExtensionProvider.GetExtension<ISamsungAppsExtensions>()
        //                 .RestoreTransactions(OnTransactionsRestored);
        //         }
        //         else if (Application.platform == RuntimePlatform.Android &&
        //             StandardPurchasingModule.Instance().appStore == AppStore.GooglePlay)
        //         {
        //             CodelessIAPStoreListener.Instance.ExtensionProvider.GetExtension<IGooglePlayStoreExtensions>()
        //                 .RestoreTransactions(OnTransactionsRestored);
        //         }
        //         else
        //         {
        //             Debug.LogWarning(Application.platform.ToString() +
        //                              " is not a supported platform for the Codeless IAP restore button");
        //         }
        //     }
        // }

        // void OnTransactionsRestored(bool success)
        // {
        //     Debug.Log("Transactions restored: " + success);
        // }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {
            Debug.Log(string.Format("IAPButton.ProcessPurchase(PurchaseEventArgs {0} - {1})", e,
                e.purchasedProduct.definition.id));

            onPurchaseComplete.Invoke(e.purchasedProduct);

            return (consumePurchase) ? PurchaseProcessingResult.Complete : PurchaseProcessingResult.Pending;
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason reason)
        {
            Debug.Log(string.Format("IAPButton.OnPurchaseFailed(Product {0}, PurchaseFailureReason {1})", product,
                reason));

            onPurchaseFailed.Invoke(product, reason);
        }

        internal void UpdateText()
        {
            // nanti update kalo udah UI pastinya


            // var product = CodelessIAPStoreListener.Instance.GetProduct(productId);
            // if (product != null)
            // {
            //     if (titleText != null)
            //     {
            //         titleText.text = product.metadata.localizedTitle;
            //     }

            //     if (descriptionText != null)
            //     {
            //         descriptionText.text = product.metadata.localizedDescription;
            //     }

            //     if (priceText != null)
            //     {
            //         priceText.text = product.metadata.localizedPriceString;
            //     }
            // }
        }
    }
}
#endif
