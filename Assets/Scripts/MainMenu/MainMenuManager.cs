﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class MainMenuManager : MonoBehaviour
{
    // [Header("TEMP LOCAL")]
    // [SerializeField] ButtonCompanyHandler _localBtnRPI;
    // [SerializeField] ButtonCompanyHandler _localBtnMINT;
    // [SerializeField] GameObject _goCanvasBooksRPILocal;
    // [SerializeField] GameObject _goCanvasBooksMINTLocal;

    [Header("General")]
    [SerializeField] bool _isTrialMode;
    [SerializeField] GameObject _gOCanvasLoading;

    [Header("Company")]
    [SerializeField] GameObject _goCanvasCompanies;
    [SerializeField] ButtonCompanyHandler _baseListTemplateCompany;

    [Header("Book")]
    [SerializeField] GameObject _goCanvasBooks;
    [SerializeField] ButtonBookHandler _baseListTemplateBook;

    [Header("AR Mode")]
    [SerializeField] GameObject _goCanvasARModeReady;
    [SerializeField] GameObject _textStatusSendMarker;
    [SerializeField] Button _buttonEmailMarker;

    public static ResponseListCompanyAndBook responseListCompanyAndBook;

    List<DataBook> _tempSelectedDataBooks;

    void Start()
    {
#if UNITY_EDITOR
        VersionHandler.UpdateAppVersion();
#endif

        // default loading di awal
        _gOCanvasLoading.SetActive(true);
        
        // Debug.LogError("TEMP LOKAL DULU");
        StartCoroutine(PostRequest("get-list", ConfigGeneral.PREFIX_API_URL + "v1/book/get-list", "{ \"id_token\":\"" + PlayerPrefs.GetString("id_token") + "\" }"));
        // if (PlayerPrefs.HasKey("companyID"))
        // {
            // switch (PlayerPrefs.GetString("companyID"))
            // {
            //     // RPI
            //     case "c8187811-b6a7-475b-bffc-704e6f5fa684":
            //         SelectCompany(_localBtnRPI);
            //         break;
            //     // MINT
            //     case "6531b9c0-9429-4751-8dd6-c90f31f0a41c":
            //         SelectCompany(_localBtnMINT);
            //         break;
            //     default :
            //         ShowCanvasCompanies();
            //         break;
            // }
        // }
        // else
        // {
            
        // }
    }

    /** ============================================ COMPANY ============================================*/
    public void ShowCanvasCompanies()
    {
        // delete saved pprefs companyID
        PlayerPrefs.DeleteKey("companyID");

        _goCanvasBooks.SetActive(false);
        _goCanvasCompanies.SetActive(true);
        _goCanvasARModeReady.SetActive(false);

        // local
        // Debug.LogError("TEMP LOKAL DULU");
        // _goCanvasBooksMINTLocal.SetActive(false);
        // _goCanvasBooksRPILocal.SetActive(false);
    }

    public void SelectCompany(ButtonCompanyHandler trgtBtn)
    {
        // set companyID
        PlayerPrefs.SetString("companyID", trgtBtn.companyID);

        // erick edited this
        StartCoroutine(GenerateCanvasBooks(trgtBtn.companyBooks));

        // Debug.LogError("LOKAL DULU");
        // ShowCanvasBooks();

        // loading hindarin double click
        _gOCanvasLoading.SetActive(true);
    }

    IEnumerator GenerateListCompany(DataCompany paramCompany)
    {
        // Generate company's thumb
        string pathFolderLocal = Application.persistentDataPath + "/Companies";
        string pathFileLocal = pathFolderLocal + "/" + paramCompany.company_cover_url;

        yield return StorageHandler.GetTextureRequestAndDownload(pathFolderLocal, pathFileLocal,
            ConfigGeneral.PREFIX_CLOUDFRONT_PUBLIC + ConfigGeneral.S3_PATH_COMPANY + paramCompany.company_cover_url, (response) =>
            {
                // Generate prefab
                GenerateItemPrefabCompany(paramCompany, response);
            });

        // save selected company
        if (PlayerPrefs.GetString("companyID") == paramCompany.company_id)
        {
            _tempSelectedDataBooks = paramCompany.company_books;
        }
    }

    void GenerateItemPrefabCompany(DataCompany paramCompany, Sprite paramSpriteImage)
    {
        ButtonCompanyHandler tempBaseList = Instantiate(_baseListTemplateCompany.gameObject, Vector3.zero, Quaternion.identity, _baseListTemplateCompany.transform.parent).GetComponent<ButtonCompanyHandler>();
        tempBaseList.SetButton(paramSpriteImage, paramCompany.company_title, paramCompany.company_id, paramCompany.company_books, (paramCompany.company_is_active == 1));

        tempBaseList.transform.localScale = Vector3.one;
        tempBaseList.gameObject.SetActive(true);
    }

    /** ============================================ BOOK ============================================*/
    public void ShowCanvasBooks()
    {
        _gOCanvasLoading.SetActive(false);
        
        _goCanvasBooks.SetActive(true);

        _goCanvasCompanies.SetActive(false);
        _goCanvasARModeReady.SetActive(false);
    }

    public void SelectBook(ButtonBookHandler trgtBtn)
    {
        // set companyID
        PlayerPrefs.SetString("bookID", trgtBtn.bookID);

        if (trgtBtn.isLocked)
        {
            // IAP
            if (trgtBtn.TryGetComponent(out UnityEngine.Purchasing.IAPButton IAPBtn))
            {
                _gOCanvasLoading.SetActive(true);
                
                IAPBtn.PurchaseProduct();
                PlayerPrefs.SetString("selectedProductID", IAPBtn.productId);
            }
        }
        else
        {
            // Debug.Log(trgtBtn.bookID);
            ShowCanvasARModeReady();
        }
    }

    IEnumerator GenerateCanvasBooks(List<DataBook> paramCompanyBooks)
    {
        // Remove current books
        foreach (Transform eachObject in _baseListTemplateBook.transform.parent.GetComponentsInChildren<Transform>(true))
            if (eachObject.name.Contains("GeneratedBook-")) Destroy(eachObject.gameObject);

        yield return null;

        // Generate this company's books
        // thisCountForColorBook = 0;
        for (int i = 0; i < paramCompanyBooks.Count; i++)
            yield return GenerateListBook(paramCompanyBooks[i]);

        

        ShowCanvasBooks();
        yield return new WaitForEndOfFrame();
    }

    IEnumerator GenerateListBook(DataBook paramBook)
    {
        // Generate company's thumb
        string pathFolderLocal = Application.persistentDataPath + "/Books";
        string pathFileLocal = pathFolderLocal + "/" + paramBook.cover_url;

        yield return StorageHandler.GetTextureRequestAndDownload(pathFolderLocal, pathFileLocal,
            ConfigGeneral.PREFIX_CLOUDFRONT_PUBLIC + ConfigGeneral.S3_PATH_BOOK + paramBook.id + "/" + paramBook.cover_url, (response) =>
            {
                // Generate prefab
                GenerateItemPrefabBook(paramBook, response);

                // Logic background color, flush to 0 if reach list_background_color's count
                // thisCountForColorBook++;
                // if (thisCountForColorBook == paramBook.list_background_color.Count) thisCountForColorBook = 0;
            });
    }

    void GenerateItemPrefabBook(DataBook paramBook, Sprite paramSpriteImage)
    {
        ButtonBookHandler tempBaseList = Instantiate(_baseListTemplateBook.gameObject, Vector3.zero, Quaternion.identity, _baseListTemplateBook.transform.parent).GetComponent<ButtonBookHandler>();
        tempBaseList.SetButton(
            paramSpriteImage, 
            paramBook.title, 
            paramBook.id,
            (_isTrialMode && paramBook.product_id != "-") ? 
                true : paramBook.book_is_active == 1, 
            (_isTrialMode) ? 
                false : (PlayerPrefs.HasKey(paramBook.id + ConfigGeneral.IS_PURCHASED)) ? 
                    false : paramBook.is_locked == 0, 
            paramBook.product_id
        );

        tempBaseList.gameObject.name = "GeneratedBook-" + paramBook.id; // For flush purpose
        tempBaseList.transform.localScale = Vector3.one;
        tempBaseList.gameObject.SetActive(true);
    }

    public void SendEmailMarker()
    {
        _buttonEmailMarker.interactable = false; // Nonactivated button to prevent multiple click
        _textStatusSendMarker.SetActive(true);

        StartCoroutine(PostRequest("send-marker-file", ConfigGeneral.PREFIX_API_URL + "v1/book/send-marker-file", "{ \"company_id\":\"" + PlayerPrefs.GetString("companyID") + "\", \"book_id\":\"" + PlayerPrefs.GetString("bookID") + "\", \"id_token\":\"" + PlayerPrefs.GetString("id_token") + "\" }"));
    }

    /** ============================================ AR MODE ============================================*/
    public void ShowCanvasARModeReady()
    {
        _goCanvasBooks.SetActive(false);
        _goCanvasCompanies.SetActive(false);
        _goCanvasARModeReady.SetActive(true);

        // Debug.LogError("LOKAL DULU");
        // _goCanvasBooksRPILocal.SetActive(false);
        // _goCanvasBooksMINTLocal.SetActive(false);
    }

    public void GoToARMode()
    {
        LoaderSceneHandler.LoadScene("SC_ARMode");
    }

    /** ============================================ API HANDLER ============================================*/
    IEnumerator PostRequest(string paramCallFrom, string paramURL, string paramJson)
    {
        var uwr = new UnityWebRequest(paramURL, "POST");
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(paramJson);
        uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        uwr.SetRequestHeader("Content-Type", "application/json");
        uwr.timeout = 10;
        yield return uwr.SendWebRequest();

        if (!uwr.isHttpError && !uwr.isNetworkError)
        {
            switch (paramCallFrom)
            {
                case "get-list":
                    responseListCompanyAndBook = ResponseListCompanyAndBook.CreateFromJSON(uwr.downloadHandler.text);

                    if (responseListCompanyAndBook.code == 200)
                    {
                        // TODO: 1) GENERATE COMPANY AND BOOKS
                        // Debug.LogError("TEMP LOKAL DULU");

                        for (int i = 0; i < responseListCompanyAndBook.data.Count; i++)
                            yield return GenerateListCompany(responseListCompanyAndBook.data[i]);

                        // 2) GENERATE CDN CONFIG FILES --> FOR GENERATE PRIVATE TO PUBLIC CLOUDFRONT URL
                        string pathCDNPolicyLocal = Application.persistentDataPath + "/Settings/" + ConfigGeneral.CLOUDFRONT_POLICY_FILE;
                        string pathCDNPrivateXMLLocal = Application.persistentDataPath + "/Settings/" + responseListCompanyAndBook.cdn_version;

                        if (!File.Exists(pathCDNPolicyLocal))
                            yield return StorageHandler.Download(ConfigGeneral.PREFIX_CLOUDFRONT_PUBLIC + "settings/" + ConfigGeneral.CLOUDFRONT_POLICY_FILE, pathCDNPolicyLocal);
                        if (!File.Exists(pathCDNPrivateXMLLocal))
                            yield return StorageHandler.Download(ConfigGeneral.PREFIX_CLOUDFRONT_PUBLIC + "settings/" + responseListCompanyAndBook.cdn_version, pathCDNPrivateXMLLocal);

                        // check if company udah di pilih / back from AR / refresh
                        if (_tempSelectedDataBooks != null)
                            StartCoroutine(GenerateCanvasBooks(_tempSelectedDataBooks));
                        else
                            _goCanvasCompanies.SetActive(true);
                    }
                    else if (responseListCompanyAndBook.code == 401)
                    {
                        // If session expired, call API Refresh Token then try again call this function
                        StartCoroutine(APIHandler.PostRequestRefreshToken());
                        yield return PostRequest(paramCallFrom, paramURL, "{ \"id_token\":\"" + PlayerPrefs.GetString("id_token") + "\" }");
                    }
                    else
                    {
                        Debug.LogError("Error: " + responseListCompanyAndBook.messages);
                        APIHandler.Logout();
                    }
                    break;
                case "send-marker-file":
                    ResponseGeneral responseGeneral = ResponseGeneral.CreateFromJSON(uwr.downloadHandler.text);
                    // TODO: responseGeneral.messages ,ditampilin di atas [Email the marker]

                    // _textStatusSendMarker.GetComponent<TMP_Text>().text = responseGeneral.messages;
                    // _textStatusSendMarker.SetActive(true);
                    _buttonEmailMarker.interactable = true;
                    break;
            }
        }
        else
        {
            Debug.LogError("Error While Sending: " + uwr.error);
            APIHandler.ConnectionErrorHandler();
        }

        _gOCanvasLoading.SetActive(false);
    }
}