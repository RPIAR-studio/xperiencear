﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Purchasing;

public class ButtonBookHandler : MonoBehaviour
{
    public string bookID, productID;
    public bool isLocked;
    [SerializeField] TMP_Text _label;
    [SerializeField] Image _imgBtn;
    [SerializeField] GameObject _goLockImg, _goUnlockImg;

    // void OnEnable()
    // {
    //     _goLockImg.SetActive(isLocked);
    //     _goUnlockImg.SetActive(!isLocked);
    // }

    void Start()
    {
        // resize sesuai screen size
        // resize harus di start setelah UI initialized
        float xScale = (Screen.width / 4.75f ) / transform.root.lossyScale.x;
        GetComponent<RectTransform>().sizeDelta = new Vector2(xScale, 0);

        // temp
        // Debug.LogError("TEMP LOKAL");
        // if (isLocked && PlayerPrefs.HasKey(productID+"_isPurchased"))
        // {
        //     Debug.LogError("UNLOCKED");
        //     isLocked = false;
        // }

        _goLockImg.SetActive(isLocked);
        _goUnlockImg.SetActive(!isLocked);
    }

    public void SetButton(Sprite imgBtn, string label, string bookID, bool isActive, bool isLocked, string productID)
    {
        this.bookID = bookID;
        this.isLocked = (!isActive) ? true : isLocked;
        _imgBtn.sprite = imgBtn;
        _label.text = label;

        // dev
        this.productID = productID;

        if (string.IsNullOrEmpty(productID))
            GetComponent<IAPButton>().enabled = false;
        else
            GetComponent<IAPButton>().productId = productID;

        GetComponent<Button>().enabled = isActive;
    }
}
