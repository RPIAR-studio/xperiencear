﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ButtonCompanyHandler : MonoBehaviour
{
    public string companyID;
    public List<DataBook> companyBooks;
    [SerializeField] TMP_Text _label;
    [SerializeField] Image _imgBtn; 
    [SerializeField] GameObject _goLockImg;

    public void SetButton(Sprite imgBtn, string label, string CompanyID, List<DataBook> CompanyBooks, bool isActive)
    {
        companyID = CompanyID;
        companyBooks = CompanyBooks;

        _imgBtn.sprite = imgBtn;
        _label.text = label;

        if (!isActive) LockThisButton();
    }

    public void LockThisButton()
    {
        _goLockImg.SetActive(true);
        this.GetComponent<Button>().interactable = false;
    }
}
