﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class APIHandler : MonoBehaviour
{
    public static void SetUserCache(DataUser paramDataUser)
    {
        PlayerPrefs.SetString("username", paramDataUser.username);
        PlayerPrefs.SetString("id_token", paramDataUser.id_token);
        PlayerPrefs.SetString("refresh_token", paramDataUser.refresh_token);
    }

    public static void Logout()
    {
        PlayerPrefs.DeleteKey("id_token");
        PlayerPrefs.DeleteKey("username");
        PlayerPrefs.DeleteKey("refresh_token");

        SceneManager.LoadScene("SC_Login");
    }

    public static void ConnectionErrorHandler()
    {
        SceneManager.LoadScene("SC_MainMenu");
    }

    public static IEnumerator PostRequestRefreshToken()
    {
        string jsonRequest = "{\"username\":\"" + PlayerPrefs.GetString("username") + "\", \"refresh_token\":\"" + PlayerPrefs.GetString("refresh_token") + "\" }";
        var uwr = new UnityWebRequest(ConfigGeneral.PREFIX_API_URL + "v2/user/refresh-token", "POST");
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(jsonRequest);
        uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        uwr.SetRequestHeader("Content-Type", "application/json");
        uwr.timeout = 10;
        yield return uwr.SendWebRequest();

        if (!uwr.isHttpError && !uwr.isNetworkError)
        {
            ResponseUserLogin responseUserLogin = ResponseUserLogin.CreateFromJSON(uwr.downloadHandler.text);
            if (responseUserLogin.code == 200)
            {
                PlayerPrefs.SetString("id_token", responseUserLogin.data.id_token); // Set new id token
                yield return null;
            }
            else
            {
                Debug.LogError("Error While Sending: " + responseUserLogin.messages);
                Logout();
            }
        }
        else
        {
            Debug.LogError("Error While Sending: " + uwr.error);
            ConnectionErrorHandler();
        }
    }

    public static string CreateCannedPrivateURL(string urlString, string durationUnits, string durationNumber, string pathToPolicyStmnt, string pathToPrivateKey, string privateKeyId)
    {
        TimeSpan timeSpanInterval = GetDuration(durationUnits, durationNumber);

        // Create the policy statement.
        string strPolicy = CreatePolicyStatement(pathToPolicyStmnt,
            urlString,
            DateTime.Now,
            DateTime.Now.Add(timeSpanInterval),
            "0.0.0.0/0");
        if ("Error!" == strPolicy) return "Invalid time frame." +
            "Start time cannot be greater than end time.";

        // Copy the expiration time defined by policy statement.
        string strExpiration = CopyExpirationTimeFromPolicy(strPolicy);

        // Read the policy into a byte buffer.
        byte[] bufferPolicy = Encoding.ASCII.GetBytes(strPolicy);

        // Initialize the SHA1CryptoServiceProvider object and hash the policy data.
        using (SHA1CryptoServiceProvider
            cryptoSHA1 = new SHA1CryptoServiceProvider())
        {
            bufferPolicy = cryptoSHA1.ComputeHash(bufferPolicy);

            // Initialize the RSACryptoServiceProvider object.
            RSACryptoServiceProvider providerRSA = new RSACryptoServiceProvider();
            XmlDocument xmlPrivateKey = new XmlDocument();

            // Load PrivateKey.xml, which you created by converting your 
            // .pem file to the XML format that the .NET framework uses.  
            // Several tools are available. 
            xmlPrivateKey.Load(pathToPrivateKey);

            // Format the RSACryptoServiceProvider providerRSA and 
            // create the signature.
            providerRSA.FromXmlString(xmlPrivateKey.InnerXml);
            RSAPKCS1SignatureFormatter rsaFormatter =
                new RSAPKCS1SignatureFormatter(providerRSA);
            rsaFormatter.SetHashAlgorithm("SHA1");
            byte[] signedPolicyHash = rsaFormatter.CreateSignature(bufferPolicy);

            // Convert the signed policy to URL-safe base64 encoding and 
            // replace unsafe characters + = / with the safe characters - _ ~
            string strSignedPolicy = ToUrlSafeBase64String(signedPolicyHash);

            // Concatenate the URL, the timestamp, the signature, 
            // and the key pair ID to form the signed URL.
            return urlString +
                "?Expires=" +
                strExpiration +
                "&Signature=" +
                strSignedPolicy +
                "&Key-Pair-Id=" +
                privateKeyId;
        }
    }

    public static string ToUrlSafeBase64String(byte[] bytes)
    {
        return System.Convert.ToBase64String(bytes)
            .Replace('+', '-')
            .Replace('=', '_')
            .Replace('/', '~');
    }

    public static TimeSpan GetDuration(string units, string numUnits)
    {
        TimeSpan timeSpanInterval = new TimeSpan();
        switch (units)
        {
            case "seconds":
                timeSpanInterval = new TimeSpan(0, 0, 0, int.Parse(numUnits));
                break;
            case "minutes":
                timeSpanInterval = new TimeSpan(0, 0, int.Parse(numUnits), 0);
                break;
            case "hours":
                timeSpanInterval = new TimeSpan(0, int.Parse(numUnits), 0, 0);
                break;
            case "days":
                timeSpanInterval = new TimeSpan(int.Parse(numUnits), 0, 0, 0);
                break;
            default:
                Console.WriteLine("Invalid time units;" +
                   "use seconds, minutes, hours, or days");
                break;
        }
        return timeSpanInterval;
    }

    public static string CreatePolicyStatement(string policyStmnt, string resourceUrl, DateTime startTime, DateTime endTime, string ipAddress)
    {
        // Create the policy statement.
        FileStream streamPolicy = new FileStream(policyStmnt, FileMode.Open, FileAccess.Read);
        using (StreamReader reader = new StreamReader(streamPolicy))
        {
            string strPolicy = reader.ReadToEnd();

            TimeSpan startTimeSpanFromNow = (startTime - DateTime.Now);
            TimeSpan endTimeSpanFromNow = (endTime - DateTime.Now);
            TimeSpan intervalStart =
               (DateTime.UtcNow.Add(startTimeSpanFromNow)) -
               new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan intervalEnd =
               (DateTime.UtcNow.Add(endTimeSpanFromNow)) -
               new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            int startTimestamp = (int)intervalStart.TotalSeconds; // START_TIME
            int endTimestamp = (int)intervalEnd.TotalSeconds;  // END_TIME

            if (startTimestamp > endTimestamp)
                return "Error!";

            // Replace variables in the policy statement.
            strPolicy = strPolicy.Replace("RESOURCE", resourceUrl);
            strPolicy = strPolicy.Replace("START_TIME", startTimestamp.ToString());
            strPolicy = strPolicy.Replace("END_TIME", endTimestamp.ToString());
            strPolicy = strPolicy.Replace("IP_ADDRESS", ipAddress);
            strPolicy = strPolicy.Replace("EXPIRES", endTimestamp.ToString());
            return strPolicy;
        }
    }

    public static string CopyExpirationTimeFromPolicy(string policyStatement)
    {
        int startExpiration = policyStatement.IndexOf("EpochTime", StringComparison.Ordinal);
        string strExpirationRough = policyStatement.Substring(startExpiration +
           "EpochTime".Length);
        char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

        List<char> listDigits = new List<char>(digits);
        StringBuilder buildExpiration = new StringBuilder(20);

        foreach (char c in strExpirationRough)
        {
            if (listDigits.Contains(c))
                buildExpiration.Append(c);
        }
        return buildExpiration.ToString();
    }
}