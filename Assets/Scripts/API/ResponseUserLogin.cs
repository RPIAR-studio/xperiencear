﻿using UnityEngine;

[System.Serializable]
public class ResponseUserLogin
{
    public int code;
    public bool success;
    public string messages;
    public bool is_need_update;
    public DataUser data;

    public static ResponseUserLogin CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<ResponseUserLogin>(jsonString);
    }
}

[System.Serializable]
public class DataUser
{
    public string username;
    public string id_token;
    public string refresh_token;
}