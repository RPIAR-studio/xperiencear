﻿using UnityEngine;

[System.Serializable]
public class ResponseGeneral
{
    public int code;
    public bool success;
    public string messages;

    public static ResponseGeneral CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<ResponseGeneral>(jsonString);
    }
}