﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResponseListCompanyAndBook
{
    public int code;
    public bool success;
    public string messages;
    public string cdn_version;
    public List<DataCompany> data;

    public static ResponseListCompanyAndBook CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<ResponseListCompanyAndBook>(jsonString);
    }
}

[System.Serializable]
public class DataCompany
{
    public string company_id;
    public string company_title;
    public int company_is_active;
    public string company_cover_url;
    public List<string> company_list_background_color;
    public List<DataBook> company_books;

}

[System.Serializable]
public class DataBook
{
    public string id;
    public string product_id;
    public string title;
    public int is_locked;
    public int book_is_active;
    public string cover_url;
    public List<string> list_background_color;

    public string asset_bundle_id;
    public string asset_bundle_name;

    public string file_name_android;
    public string file_name_android_deleted;
    public float file_size_android;

    public string file_name_ios;
    public string file_name_ios_deleted;
    public float file_size_ios;

}