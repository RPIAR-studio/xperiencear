﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoaderSceneHandler : MonoBehaviour
{
    public static string nextSceneName;

    float _delay = 1.5f;

    public static void LoadScene(string sceneName)
    {
        nextSceneName = sceneName;

        SceneManager.LoadScene("SC_Loading");
    }

    IEnumerator Start()
    {
        yield return new WaitForSeconds(_delay);

        SceneManager.LoadScene(nextSceneName);
    }
}
