﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchableObjMatTag : MonoBehaviour
{
    // tag
    public string textureID;
    public string customTextureTag;
}
