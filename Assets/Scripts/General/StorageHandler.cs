﻿using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class StorageHandler : MonoBehaviour
{
    public static IEnumerator Download(string paramURL, string paramLocalPathFileName)
    {
        using (UnityWebRequest uwr = UnityWebRequest.Get(paramURL))
        {
            uwr.downloadHandler = new DownloadHandlerFile(paramLocalPathFileName);
            uwr.timeout = 60;
            yield return uwr.SendWebRequest();

            if (!uwr.isHttpError && !uwr.isNetworkError) yield return null;
            else
            {
                Debug.LogError("Error While Sending: " + uwr.error);
                yield return null;
            }
        }
    }

    public static IEnumerator GetTextureRequest(string url, System.Action<Sprite> callback)
    {
        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(url))
        {
            yield return uwr.SendWebRequest();
            if (!uwr.isHttpError && !uwr.isNetworkError)
            {
                Texture2D texture = ((DownloadHandlerTexture)uwr.downloadHandler).texture;
                Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
                callback(sprite);
            }
            else
            {
                Debug.LogError("Error While Sending: " + uwr.error);
                yield return new WaitForEndOfFrame();
            }
        }
    }

    public static IEnumerator GetTextureRequestAndDownload(string pathFolderLocal, string pathFileLocal, string cloudfrontUrl, System.Action<Sprite> callback)
    {
        if (File.Exists(pathFileLocal))
        {
            Texture2D texture = LoadPNG(pathFileLocal);
            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
            callback(sprite);
        }
        else
        {
            using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(cloudfrontUrl))
            {
                yield return uwr.SendWebRequest();
                if (!uwr.isHttpError && !uwr.isNetworkError)
                {
                    Texture2D texture = ((DownloadHandlerTexture)uwr.downloadHandler).texture;

                    // Save image to local
                    SaveImage(texture, pathFolderLocal, pathFileLocal);

                    Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
                    callback(sprite);
                }
                else
                {
                    Debug.LogError("Error While Sending: " + uwr.error);
                    yield return new WaitForEndOfFrame();
                }
            }
        }
    }

    public static string LoadData(string paramLocalPathFileName)
    {
        return File.ReadAllText(paramLocalPathFileName);
    }

    public static void SaveData(string paramLocalPathFileName, string paramDataJSON)
    {
        File.WriteAllText(paramLocalPathFileName, paramDataJSON);
    }

    public static Texture2D LoadPNG(string paramFilePath)
    {
        Texture2D texture = null;
        byte[] fileData;

        if (File.Exists(paramFilePath))
        {
            fileData = File.ReadAllBytes(paramFilePath);
            texture = new Texture2D(2, 2);
            if (texture.LoadImage(fileData))  // Load the imagedata into the texture (size is set automatically)
                return texture;
            texture.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return null;
    }

    public static void SaveImage(Texture2D paramTexture, string paramFolderLocal, string paramFileLocal)
    {
        var imageBytes = paramTexture.EncodeToPNG();
        if (!Directory.Exists(paramFolderLocal)) Directory.CreateDirectory(paramFolderLocal);
        File.WriteAllBytes(paramFileLocal, imageBytes);
    }

    public static void SaveFile(UnityWebRequest www, string paramFolderLocal, string paramFileLocal)
    {
        if (!Directory.Exists(paramFolderLocal)) Directory.CreateDirectory(paramFolderLocal);
        File.WriteAllBytes(paramFileLocal, www.downloadHandler.data);
    }
}