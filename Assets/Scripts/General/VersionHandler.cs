﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;

public class VersionHandler
{
#if UNITY_EDITOR
        public static void UpdateAppVersion()
        {
            string path = Application.streamingAssetsPath + "/" + ConfigGeneral.APP_VERSION_FILE;

            StreamWriter writer = new StreamWriter(path);
            
            writer.Write(Application.version);
            writer.Close();

            UnityEditor.AssetDatabase.Refresh();
        }
#endif

    public static IEnumerator GetAppVersionRoutine()
    {
        string filePath = Application.streamingAssetsPath + "/" + ConfigGeneral.APP_VERSION_FILE;

        yield return null;

        if (filePath.Contains("://"))
        {
            using (UnityWebRequest data = UnityWebRequest.Get(filePath))
            {
                yield return data.SendWebRequest();

                if (string.IsNullOrEmpty(data.error))
                    PlayerPrefs.SetString(ConfigGeneral.APP_VERSION_FILE, data.downloadHandler.text);
            }
        }
        else
        {
            PlayerPrefs.SetString(ConfigGeneral.APP_VERSION_FILE, File.ReadAllText(filePath));
        }
    }
}