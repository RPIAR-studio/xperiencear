﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SplashManager : MonoBehaviour
{
    [SerializeField] TMP_Text _textVersion;
    [SerializeField] string _nextSceneName;
    [SerializeField] float _delay = 2;

    IEnumerator Start() 
    {
        // jaga2 bisi ada yg nyangkut
        if (!PlayerPrefs.HasKey("flush_002"))
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.SetInt("flush_002", 1);
        }
        
        yield return VersionHandler.GetAppVersionRoutine();

        _textVersion.text = string.Format("v. {0}", PlayerPrefs.GetString(ConfigGeneral.APP_VERSION_FILE));

        yield return new WaitForSeconds(_delay);

        LoaderSceneHandler.LoadScene(_nextSceneName);
    }
}
