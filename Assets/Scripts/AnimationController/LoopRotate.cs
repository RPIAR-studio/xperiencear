﻿using UnityEngine;

namespace MorphooStudios.Animation
{
    public class LoopRotate : MonoBehaviour
    {
        [SerializeField] Vector3 _rotSpeed;

        void LateUpdate()
        {
            transform.Rotate(_rotSpeed * Time.deltaTime);
        }
    }
}
