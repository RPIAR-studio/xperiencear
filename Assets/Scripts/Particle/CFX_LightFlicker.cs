using UnityEngine;
using System.Collections;

// Randomly changes a light's intensity over time.

[RequireComponent(typeof(Light))]
public class CFX_LightFlicker : MonoBehaviour
{
	// Loop flicker effect
	public bool loop;
	public float smoothFactor = 1f;
	
	public float addIntensity = 1.0f;
	
	private float minIntensity;
	private float maxIntensity;
	private float baseIntensity;
	
	void Awake()
	{
		baseIntensity = GetComponent<Light>().intensity;
	}
	
	void OnEnable()
	{
		minIntensity = baseIntensity;
		maxIntensity = minIntensity + addIntensity;
	}
	
	void Update ()
	{
		GetComponent<Light>().intensity = Mathf.Lerp(minIntensity, maxIntensity, Mathf.PerlinNoise(Time.time * smoothFactor, 0f));
	}
}
