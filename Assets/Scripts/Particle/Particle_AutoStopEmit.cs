﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle_AutoStopEmit : MonoBehaviour
{
    [SerializeField] float timeEmit = 3;

    void Start()
    {
        StartCoroutine("CheckRoutine");
	}
	
	IEnumerator CheckRoutine ()
	{
		ParticleSystem ps = this.GetComponent<ParticleSystem>();
		
		yield return new WaitForSeconds(timeEmit);

        var emission = ps.emission;
        emission.enabled = false;
    }
}
