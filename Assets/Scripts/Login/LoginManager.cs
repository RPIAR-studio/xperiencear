﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LoginManager : MonoBehaviour
{
    [Header("General")]
    [SerializeField] GameObject _generalLoading;
    [SerializeField] GameObject _generalNotification;
    [SerializeField] TMP_Text _generalNotificationMessage;

    [Header("List Panel")]
    [SerializeField] GameObject _panelMain;
    [SerializeField] GameObject _panelLogin;
    [SerializeField] GameObject _panelRegisterInputUsername, _panelRegisterInputPassword;
    [SerializeField] GameObject _panelRegisterAuth, _panelRegisterSuccess;
    [SerializeField] GameObject _panelForgotPassword;

    [Header("Panel Login")]
    [SerializeField] TMP_InputField _email;
    [SerializeField] TMP_InputField _password;

    [Header("Panel Register")]
    [SerializeField] TMP_InputField _usernameRegister;
    [SerializeField] TMP_InputField _passwordRegister;

    [Header("Panel Register Auth")]
    [SerializeField] TMP_Text _emailAuth;
    [SerializeField] Button _buttonResend;
    [SerializeField] TMP_InputField[] _inputFieldsVerificationRegister;
    [SerializeField] Button _buttonSubmitVerificationRegister;
    string verificationCodeRegister;

    [Header("Panel Forgot Password")]
    [SerializeField] GameObject[] _baseStepsForgotPassword;
    [SerializeField] TMP_InputField _newPasswordForgotPassword;
    [SerializeField] TMP_InputField[] _inputFieldsVerificationForgotPassword;
    [SerializeField] Button _buttonSubmitVerificationForgotPassword;
    string verificationCodeForgotPassword;

    const string ALERT_NO_CONNECTION = "No internet connection. Make sure that Wi-Fi or mobile data is turned on, then try again.";
    const string ALERT_COMPLETE_INPUT_FIELD = "Please complete the input field!";

    bool _isNoInternetConection, _isNeedUpdate;
    string _tempSavedPass, _tempSavedUsername;

    void Start()
    {
        // by default hapus company
        PlayerPrefs.DeleteKey("companyID");

        StartCoroutine(PostRequest("check-token", ConfigGeneral.PREFIX_API_URL + "v1/user/check-token",
            "{ \"app_version\":\"" + PlayerPrefs.GetString(ConfigGeneral.APP_VERSION_FILE) + "\"," +
            "\"id_token\":\"" + PlayerPrefs.GetString("id_token") + "\"," +
            "\"id_token\":\"" + PlayerPrefs.GetString("id_token") + "\"," +
            "\"refresh_token\":\"" + PlayerPrefs.GetString("refresh_token") + "\" }"));
    }

    /** ============================================ GENERAL ============================================*/
    void GoToMainMenu()
    {
        LoaderSceneHandler.LoadScene("SC_MainMenu");
    }

    public void ShowNotification(string paramMessage)
    {
        _generalNotificationMessage.text = paramMessage;
        _generalNotification.SetActive(true);
    }

    public void BackToPageAfterNotication()
    {
        if (_isNeedUpdate)
        {
            switch (Application.platform)
            {   
                case RuntimePlatform.Android:
                    Application.OpenURL("market://details?id=com.RPIeSolutios.XperienceAR");
                break;
                case RuntimePlatform.IPhonePlayer:
                    Application.OpenURL("itms-apps://itunes.apple.com/app/id376771144");
                break;
            }
            return;
        }

        _generalNotification.SetActive(false);

        // no internet connection
        if (_isNoInternetConection)
            LoaderSceneHandler.LoadScene("SC_Login");
    }

    public void OpenURL(string paramURL)
    {
        Application.OpenURL(paramURL);
    }

    void SaveLocalLoginCreds(string username, string pass)
    {
        _tempSavedUsername = username;
        _tempSavedPass = pass;
    }

    /** ============================================ LOGIN ============================================*/
    public void CheckUserLogin()
    {
        if (_email.text == "") ShowNotification(ALERT_COMPLETE_INPUT_FIELD);
        else
        {
            // Check user exist/ not
            StartCoroutine(PostRequest("check-exist", ConfigGeneral.PREFIX_API_URL + "v1/user/check-exist", "{ \"username\":\"" + _email.text + "\" }"));
        }
    }

    public void GoToRegister()
    {
        if (_email.text == "") ShowNotification(ALERT_COMPLETE_INPUT_FIELD);
        else
        {
            _panelMain.SetActive(false);
            _panelRegisterInputUsername.SetActive(true);
        }
    }

    public void GoToPanelMain(GameObject paramFromPanel)
    {
        PlayerPrefs.DeleteKey("id_token");

        paramFromPanel.SetActive(false);
        _panelMain.SetActive(true);
    }

    public void DoLogin()
    {
        if (_password.text == "")
            ShowNotification(ALERT_COMPLETE_INPUT_FIELD);
        else
        {
            SaveLocalLoginCreds(_email.text, _password.text);
            StartCoroutine(PostRequest("login", ConfigGeneral.PREFIX_API_URL + "v1/user/login", "{ \"username\":\"" + _email.text + "\", \"password\":\"" + _password.text + "\" }"));
        }
    }

    public void DoAutoLogin()
    {
        if (!string.IsNullOrEmpty(_tempSavedUsername) && !string.IsNullOrEmpty(_tempSavedPass))
            StartCoroutine(PostRequest("login", ConfigGeneral.PREFIX_API_URL + "v1/user/login", "{ \"username\":\"" + _tempSavedUsername + "\", \"password\":\"" + _tempSavedPass + "\" }"));
        else
            LoaderSceneHandler.LoadScene("SC_Login"); // restart
    }

    /** ============================================ REGISTER ============================================*/
    public void GoToRegisterInputUsername()
    {
        _panelRegisterInputUsername.SetActive(true);
        _panelRegisterInputPassword.SetActive(false);
    }

    public void GoToRegisterInputPassword()
    {
        if (_usernameRegister.text == "")
            ShowNotification(ALERT_COMPLETE_INPUT_FIELD);
        else
        {
            _panelRegisterAuth.SetActive(false);
            _panelRegisterInputUsername.SetActive(false);
            _panelRegisterInputPassword.SetActive(true);
            _passwordRegister.text = "";
        }
    }

    public void SetVerificationRegisterCode(int curBox)
    {
        verificationCodeRegister = null;

        StartCoroutine(SetVerificationRegisterCodeRoutine(curBox));
    }

    IEnumerator SetVerificationRegisterCodeRoutine(int curBox)
    {
        if (!string.IsNullOrEmpty(_inputFieldsVerificationRegister[curBox].text))
        {
            _inputFieldsVerificationRegister[curBox].DeactivateInputField(true);

            yield return (Application.platform == RuntimePlatform.IPhonePlayer) ? null : new WaitForSeconds(.1f);

            if (curBox < _inputFieldsVerificationRegister.Length - 1)
            {
                _inputFieldsVerificationRegister[curBox + 1].ActivateInputField();
                _inputFieldsVerificationRegister[curBox + 1].Select();
            }
            else
                _inputFieldsVerificationRegister[curBox].DeactivateInputField(true);
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Backspace) && curBox > 0)
                _inputFieldsVerificationRegister[curBox - 1].Select();
        }

        foreach (var item in _inputFieldsVerificationRegister)
        {
            verificationCodeRegister += item.text;
        }

        yield return null;

        _buttonSubmitVerificationRegister.interactable = (verificationCodeRegister.Length == 6);
    }

    public void DoRegister()
    {
        if (_passwordRegister.text == "")
            ShowNotification(ALERT_COMPLETE_INPUT_FIELD);
        else
        {
            SaveLocalLoginCreds(_email.text, _passwordRegister.text);
            StartCoroutine(PostRequest("signup", ConfigGeneral.PREFIX_API_URL + "v1/user/signup",
                "{ \"first_name\":\"" + _usernameRegister.text + "\"" +
                ", \"email\":\"" + _email.text + "\"" +
                ", \"password\":\"" + _passwordRegister.text + "\"}"));
        }
    }

    public void DoRegisterAuth()
    {
        StartCoroutine(PostRequest("signup-confirmation", ConfigGeneral.PREFIX_API_URL + "v1/user/signup-confirmation", "{ \"username\":\"" + PlayerPrefs.GetString("username") + "\", \"confirmation_code\":\"" + verificationCodeRegister + "\" }"));
    }

    public void DoResendRegisterAuth()
    {
        _buttonResend.interactable = false; // Nonactivated button resend to prevent multiple click
        StartCoroutine(PostRequest("signup-confirmation-resend", ConfigGeneral.PREFIX_API_URL + "v1/user/signup-resend-code", "{ \"username\":\"" + PlayerPrefs.GetString("username") + "\" }"));
    }

    /** ============================================ FORGOT PASSWORD ============================================*/
    public void DoForgotPassword()
    {
        StartCoroutine(PostRequest("forgot-password", ConfigGeneral.PREFIX_API_URL + "v1/user/forgot-password", "{ \"username\":\"" + _email.text + "\" }"));
    }

    public void SetVerificationForgotPasswordCode(int curBox)
    {
        verificationCodeForgotPassword = null;

        StartCoroutine(SetVerificationForgotPasswordCodeRoutine(curBox));
    }

    IEnumerator SetVerificationForgotPasswordCodeRoutine(int curBox)
    {
        if (!string.IsNullOrEmpty(_inputFieldsVerificationForgotPassword[curBox].text))
        {
            _inputFieldsVerificationForgotPassword[curBox].DeactivateInputField(true);

            yield return (Application.platform == RuntimePlatform.IPhonePlayer) ? null : new WaitForSeconds(.1f);

            if (curBox < _inputFieldsVerificationForgotPassword.Length - 1)
            {
                _inputFieldsVerificationForgotPassword[curBox + 1].ActivateInputField();
                _inputFieldsVerificationForgotPassword[curBox + 1].Select();
            }
            else
                _inputFieldsVerificationForgotPassword[curBox].DeactivateInputField(true);
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Backspace) && curBox > 0)
                _inputFieldsVerificationForgotPassword[curBox - 1].Select();
        }

        foreach (var item in _inputFieldsVerificationForgotPassword)
        {
            verificationCodeForgotPassword += item.text;
        }

        yield return null;

        _buttonSubmitVerificationForgotPassword.interactable = (verificationCodeForgotPassword.Length == 6);
    }

    public void CheckPasswordForgotPasswordLength(string pass)
    {
        _buttonSubmitVerificationForgotPassword.interactable = pass.Length > 5;
    }

    public void DoForgotPasswordConfirmation()
    {
        if (_newPasswordForgotPassword.text == "")
            ShowNotification(ALERT_COMPLETE_INPUT_FIELD);
        else
        {
            // check step
            if (!_baseStepsForgotPassword[1].activeSelf)
            {
                _baseStepsForgotPassword[0].SetActive(false);
                _baseStepsForgotPassword[1].SetActive(true);

                return;
            }

            SaveLocalLoginCreds(_email.text, _newPasswordForgotPassword.text);
            StartCoroutine(PostRequest("confirm-forgot-password", ConfigGeneral.PREFIX_API_URL + "v1/user/confirm-forgot-password", "{ \"username\":\"" + _email.text + "\", \"confirmation_code\":\"" + verificationCodeForgotPassword + "\", \"new_password\":\"" + _newPasswordForgotPassword.text + "\" }"));
        }
    }

    /** ============================================ API HANDLER ============================================*/
    IEnumerator PostRequest(string paramCallFrom, string paramURL, string paramJson)
    {
        _generalLoading.SetActive(true);

        var uwr = new UnityWebRequest(paramURL, "POST");
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(paramJson);
        uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        uwr.SetRequestHeader("Content-Type", "application/json");
        uwr.timeout = 10;
        yield return uwr.SendWebRequest();

        if (!uwr.isHttpError && !uwr.isNetworkError)
        {
            ResponseUserLogin responseUserLogin;
            ResponseGeneral responseGeneral;

            switch (paramCallFrom)
            {
                case "check-token":
                    responseUserLogin = ResponseUserLogin.CreateFromJSON(uwr.downloadHandler.text);
                    if (responseUserLogin.code == 200)
                    {
                        // If 200, set id token and open scene main menu
                        PlayerPrefs.SetString("id_token", responseUserLogin.data.id_token);
                        GoToMainMenu();
                    }
                    else if (responseUserLogin.code == 201)
                    {
                        // If need update app, show this notif
                        if (responseUserLogin.is_need_update)
                        {
                            _isNeedUpdate = true;

                            _generalLoading.SetActive(false);

                            ShowNotification("You're using an old version of the ExperienceAR App. Please update to access its latest features and improved app stability.");

                            yield break;
                        }
                        else ShowNotification(responseUserLogin.messages);
                    }
                    else
                    {
                        PlayerPrefs.DeleteKey("id_token");
                        _panelMain.SetActive(true);
                    }
                    break;
                case "check-exist":
                    responseGeneral = ResponseGeneral.CreateFromJSON(uwr.downloadHandler.text);
                    if (responseGeneral.code == 200)
                    {
                        // If 200, go to panel login - password
                        _panelMain.SetActive(false);
                        _panelLogin.SetActive(true);
                        _password.text = "";
                    }
                    else ShowNotification(responseGeneral.messages);
                    break;
                case "login":
                    responseUserLogin = ResponseUserLogin.CreateFromJSON(uwr.downloadHandler.text);
                    if (responseUserLogin.code == 200)
                    {
                        // If 200, set user data to cache
                        APIHandler.SetUserCache(responseUserLogin.data);
                        GoToMainMenu();
                    }
                    else if (responseUserLogin.code == 401)
                    {
                        PlayerPrefs.SetString("username", responseUserLogin.data.username);

                        // Do resend confirmation code
                        StartCoroutine(PostRequest("signup-confirmation-resend-when-login", ConfigGeneral.PREFIX_API_URL + "v1/user/signup-resend-code", "{ \"username\":\"" + PlayerPrefs.GetString("username") + "\" }"));
                    }
                    else ShowNotification(responseUserLogin.messages);
                    break;
                case "signup":
                    responseUserLogin = ResponseUserLogin.CreateFromJSON(uwr.downloadHandler.text);
                    if (responseUserLogin.code == 200)
                    {
                        _panelMain.SetActive(false);

                        // If 200, go to panel verfication code
                        _panelRegisterInputPassword.SetActive(false);
                        PlayerPrefs.SetString("username", responseUserLogin.data.username);
                        _emailAuth.SetText(responseUserLogin.data.username);
                        _panelRegisterAuth.SetActive(true);
                        _buttonResend.interactable = true;
                    }
                    else ShowNotification(responseUserLogin.messages);
                    break;
                case "signup-confirmation":
                    responseGeneral = ResponseGeneral.CreateFromJSON(uwr.downloadHandler.text);
                    if (responseGeneral.code == 200)
                    {
                        // If 200, go to panel register success
                        _panelRegisterAuth.SetActive(false);
                        _panelRegisterSuccess.SetActive(true);
                        _email.text = "";
                        _password.text = "";
                    }
                    else ShowNotification(responseGeneral.messages);
                    break;
                case "signup-confirmation-resend":
                    responseGeneral = ResponseGeneral.CreateFromJSON(uwr.downloadHandler.text);
                    if (responseGeneral.code != 200) ShowNotification(responseGeneral.messages);
                    break;
                case "signup-confirmation-resend-when-login":
                    responseUserLogin = ResponseUserLogin.CreateFromJSON(uwr.downloadHandler.text);
                    if (responseUserLogin.code == 200)
                    {
                        // If 200, go to panel verfication code
                        _panelLogin.SetActive(false);
                        _panelRegisterAuth.SetActive(true);
                        _emailAuth.SetText(responseUserLogin.data.username);
                    }
                    else ShowNotification(responseUserLogin.messages);
                    break;
                case "forgot-password":
                    responseGeneral = ResponseGeneral.CreateFromJSON(uwr.downloadHandler.text);
                    if (responseGeneral.code == 200)
                    {
                        // If 200, go to panel confirm forgotten password
                        _panelLogin.SetActive(false);
                        _panelForgotPassword.SetActive(true);
                        _newPasswordForgotPassword.text = "";
                    }
                    else ShowNotification(responseGeneral.messages);
                    break;
                case "confirm-forgot-password":
                    responseGeneral = ResponseGeneral.CreateFromJSON(uwr.downloadHandler.text);
                    if (responseGeneral.code == 200)
                    {
                        // If 200, go to login menu
                        _panelForgotPassword.SetActive(false);
                        DoAutoLogin();
                    }
                    else ShowNotification(responseGeneral.messages);
                    break;
            }
        }
        else
        {
            _isNoInternetConection = true;
            // no internet connection warning
            ShowNotification(ALERT_NO_CONNECTION);
        }
        _generalLoading.SetActive(false);
    }
}