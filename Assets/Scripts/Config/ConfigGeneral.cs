﻿public static class ConfigGeneral
{
    public const string APP_VERSION_FILE = "XperienceAR";

    // API & Cloudfront URL
    public const string PREFIX_API_URL = "https://wc5mali3ba.execute-api.ap-southeast-1.amazonaws.com/XperienceAR/app/";
    public const string PREFIX_CLOUDFRONT_PUBLIC = "https://dxk7qake616ce.cloudfront.net/";
    public const string PREFIX_CLOUDFRONT_PRIVATE = "https://d3q3olxxtzo9c2.cloudfront.net/";
    
    // Cloudfront setting // TODO
    public const string CLOUDFRONT_KEY_PAIR_ID = "AKIAV5HLFTYI5IVULH45";
    public const string CLOUDFRONT_POLICY_FILE = "CannedPolicy.txt";
    public const string CLOUDFRONT_MINUTE_EXPIRED = "15";

    // S3 prefix path
    public const string S3_PATH_ASSET_BUNDLE = "assetbundles/";
    public const string S3_PATH_COMPANY = "companies/";
    public const string S3_PATH_BOOK = "books/";

    // General
    public const string IS_PURCHASED = "_isPurchased";

    public static string Platform()
    {
        return (UnityEngine.Application.platform == UnityEngine.RuntimePlatform.IPhonePlayer) ? "apple" : "android";
    }
}