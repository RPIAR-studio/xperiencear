#if UNITY_PURCHASING || UNITY_UNIFIED_IAP

using UnityEditor;
using UnityEngine;
using UnityEngine.Purchasing;
using System.IO;
using System.Collections.Generic;

namespace UnityEditor.Purchasing
{
    public static class IAPListenerMenu
    {
        [MenuItem("GameObject/Unity IAP/IAP Listener", false, 10)]
        public static void GameObjectCreateUnityIAPListener()
        {
            CreateUnityIAPListener();
        }

        [MenuItem ("Window/Unity IAP/Create IAP Listener", false, 6)]
        public static void CreateUnityIAPListener()
        {
            EditorApplication.ExecuteMenuItem("GameObject/Create Empty");

            GameObject gO = Selection.activeGameObject;

            if (gO) {
                gO.AddComponent<IAPListener>();
                gO.name = "IAP Listener";
            }
        }
    }


	[CustomEditor(typeof(IAPButton))]
	[CanEditMultipleObjects]
	public class IAPButtonEditor : Editor
	{
		private static readonly string[] excludedFields = new string[] { "m_Script" };
		private static readonly string[] restoreButtonExcludedFields = new string[] { "m_Script", "consumePurchase", "onPurchaseComplete", "onPurchaseFailed", "titleText", "descriptionText", "priceText" };
		private const string kNoProduct = "<None>";

		private List<string> m_ValidIDs = new List<string>();

		public override void OnInspectorGUI()
		{
			IAPButton button = (IAPButton)target;

			serializedObject.Update();

			// if (button.buttonType == IAPButton.ButtonType.Purchase) {
				EditorGUILayout.LabelField(new GUIContent("Product ID:", "Select a product from the IAP catalog"));

				var catalog = ProductCatalog.LoadDefaultCatalog();

				m_ValidIDs.Clear();
				m_ValidIDs.Add(kNoProduct);
				foreach (var product in catalog.allProducts) {
					m_ValidIDs.Add(product.id);
				}

				if (GUILayout.Button("IAP Catalog...")) {
					ProductCatalogEditor.ShowWindow();
				}
			// }

			// DrawPropertiesExcluding(serializedObject, button.buttonType == IAPButton.ButtonType.Restore ? restoreButtonExcludedFields : excludedFields);
			DrawPropertiesExcluding(serializedObject, excludedFields);

			serializedObject.ApplyModifiedProperties();
		}
	}
}
#endif
