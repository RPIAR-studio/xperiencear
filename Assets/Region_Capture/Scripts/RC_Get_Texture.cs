﻿using UnityEngine;

public class RC_Get_Texture : MonoBehaviour {

	public Camera RenderCamera;
	[Space(20)]
	public bool FreezeEnable = false;
    // private bool SkinnedMesh;

    private RenderTexture _rt;

    // additional multiple mats
    [Header("Multiple mats setting")]
    [SerializeField] int _matIndex;
    [SerializeField] bool _allMats = true; // set by default

    void Start () 
	{
		if (FreezeEnable && RenderCamera) RenderCamera.enabled = false;
        // if (GetComponent<SkinnedMeshRenderer>()) SkinnedMesh = true;
    }

	private void Update() 
	{
        if (!RenderCamera) return;

        if (!_rt && RenderCamera.targetTexture)
        {
            _rt = RenderCamera.targetTexture;

            Renderer item = GetComponent<Renderer>();

            if (_allMats)
                for (int i = 0; i < item.materials.Length; i++)
                    item.materials[i].SetTexture("_MainTex", RenderCamera.targetTexture);
            else
                item.materials[_matIndex].SetTexture("_MainTex", RenderCamera.targetTexture);

            // if (SkinnedMesh) 
            // {
            //     SkinnedMeshRenderer item = GetComponent<SkinnedMeshRenderer>();

            //     if (_allMats)
            //         for (int i = 0; i < item.materials.Length; i++)
            //             item.materials[i].SetTexture("_MainTex", RenderCamera.targetTexture);
            //     else
            //         item.materials[_matIndex].SetTexture("_MainTex", RenderCamera.targetTexture);


            //     // GetComponent<SkinnedMeshRenderer>().material.SetTexture("_MainTex", RenderCamera.targetTexture);
            // }
            // else 
            // {
            //     MeshRenderer item = GetComponent<MeshRenderer>();

            //     if (_allMats)
            //         for (int i = 0; i < item.materials.Length; i++)
            //             item.materials[i].SetTexture("_MainTex", RenderCamera.targetTexture);
            //     else
            //         item.materials[_matIndex].SetTexture("_MainTex", RenderCamera.targetTexture);

            //     // GetComponent<MeshRenderer>().material.SetTexture("_MainTex", RenderCamera.targetTexture);
            // }
        }

        if (_rt && _rt != RenderCamera.targetTexture) Destroy(_rt);
    }
		
	void onGUI () 
	{
		if (RenderCamera)
		{
            if (FreezeEnable) RenderCamera.enabled = false;
			else RenderCamera.enabled = true;
		}
	}
}